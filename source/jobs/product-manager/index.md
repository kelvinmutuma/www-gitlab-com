---
layout: markdown_page
title: "Product Manager Responsibilities and Tasks"
---

The product manager reports to the CEO and is an individual contributor.

## Development direction

- Manage CE / EE / CI features: make sure a release contains attractive new features.
- Manage customer requests: take load of support for handling this and making sure we deliver
- Create direction in development efforts: Making sure that our long term goals are closer with each release
- Make sure other products (license, version) are not snowed under with the release
- Handle feature requests from sales
- Ensure that we translate user demands in features that make them happy but keep the product UI clean and the codebase maintainable
- Ensure that the current and next milestone is contain the most relevant items and will be realized
- Ensure that there is a bigger vision for the product, like the [direction page](https://about.gitlab.com/direction/)
-  Ensure that GitLab has the functionality to run GitLab.com (e.g. spam fighting)
- The CTO has a final say on all features, the [CEO will also be involved in many decisions](http://www.bhorowitz.com/why_founders_fail_the_product_ceo_paradox).

## Public information

- Guard the quality of any public materials, such as documentation and tweets
- keep the feature request tracker up to date
- Make sure developers write good docs for every function
- Make sure the CE/EE comparison, GitHub comparison and battlecards
are up to date
- Ensure that answers to internal questions end up being documented publicly
- Ensure that everyone inside and outside company knows what GitLab can do and what we're working on
- Move most of the issues from dev to .com (so our internal roadmap is public)
- Ensure all our knowledge is radiated to users (prevent us from running different code than the default)
- Make [/direction](/direction) a high level roadmap (can link to issues on .com)

## Customer relations

- Join customer visits if they can lead to new features
- Join partner visits if they can lead to new features
- Make sure the demo scripts are up to date and train people
- Move feature forum to GitLab issues (once [award emoji](https://dev.gitlab.org/gitlab/gitlabhq/issues/2388) is out)

## Marketing

- Make sure the release announcements are well written and cover everything
- Do regular feature highlights
- Ensure sales has proper marketing materials for our features
- Answer feature questions on social media (twitter, stack overflow, mailinglist)
- Make sure the website is effective (good content, well presented)
- Ensure that we communicate our product strengths (on our website, social media, release posts, other blog posts, sales playbook,
conferences, webinars, etc.) together with the VP of Marketing
- Make sure the website infrastructure is effective, including being the lead for [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/)

## Internal

- Ensure that salespeople are trained in and tested on our product
- Ensure issue and bugs don't fall through the cracks
- Ensure the release process is well documented

## Things I'm doing that I shouldn't be doing

I do like to do these things, but it's probably best if someone takes over.

- Maintain about.gitlab.com (content, code, design, deploy)
- Maintain version.gitlab.com
- ~~Maintain doc.gitlab.com (code/deploy)~~ => Drew
- ~~Maintain Gitlab Artwork~~ => Andriy
- Post + Reply to things on twitter
- ~~Reply to Disqus comments~~ => Forwarded to service engineers
- Maintain the [AWS](https://dev.gitlab.org/gitlab/AMI) and (theoretical) [Azure](https://dev.gitlab.org/gitlab/organization/issues/204) images
- Be on call for GitLab.com and Support emergencies
- Be support back up / handle European calls
- Maintain Google Analytics, Mixpanel
- Ownership of Google Ads (to move to Ashley)
- Ownership of Twitter Ads (to move to Ashley)
- Interview candidates for Interaction Engineer + Designer positions (I like this as well, but time sink)
- Maintain CF Tile (this is upcoming)
- Maintain the mailinglists + automation
- Give trainings to customers
- Maintain automations (Slack, Notifiers, Zendesk)
