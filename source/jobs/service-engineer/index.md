---
layout: markdown_page
title: "Service Engineer Responsibilities and Tasks"
---

## Responsibilities

* Engage with our customers—anything from a small advertising firm or a university, to Fortune 100 clients
* Communicate via email and video conferencing with potential clients
* Assist with questions coming from Twitter and Facebook
* Respond to questions on the GitLab.com Support Forum
* Engage with the rest of the core team to improve GitLab
* Fix problems, add features, improve documentation, and polish the website
* Engage with people on various forums
